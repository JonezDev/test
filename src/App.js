import logo from './logo.svg';
import './App.css';
import Navbar from './Navbar'
import MainPage from './MainPage'
import Page2 from './Page2'
import Background from './Background'
import Footer from './Footer'
import { createTheme,ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';

const theme = createTheme({
  typography: {
    fontFamily: [
      'Metrophobic',
      'sans-serif',
    ].join(','),
    button: {
      textTransform: 'none'
    }
  },});
function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Box sx={{ zIndex: 'snackbar' }}>
        <Background/>
        </Box>
        <Box sx={{ zIndex: 'tooltip' }}>
        <Navbar/>
        <MainPage/>
        <Page2/>
        <Footer/>
        </Box>
        
        
      </div>
    </ThemeProvider>
  );
}

export default App;
