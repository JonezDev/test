import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import Eye from './img/eye.png'
import Chart from './img/chart.png'
import Newsletter from './img/newsletter.png'
import Timeline from './img/timeline.png'
import GroupIcon from './img/GroupIcon.png'
import GroupIcon2 from './img/GroupIcon2.png'
import GroupButton from './img/GroupButton.png'
import MainImage from './img/MainImage.png'
import compass from './img/compass.png'
import Profile from './img/Profile.png'
import Logo_5 from './img/logo_5.png'
import logo_4 from './img/logo_4.png'
import Divider from '@mui/material/Divider';
import TextField from '@mui/material/TextField';
import { GoogleLogin, GoogleLogout } from "react-google-login";
import GoogleButton from 'react-google-button'
function ResponsiveAppBar() {


  return (
    <>
        
          <Grid  container  direction="column" justifyContent={'center'}  xs={12} sx={{ zIndex: '1000',width:'100%',mt:'482px'}}>
            <Grid item xs={12}>
            <Button  sx={{borderRadius:10,px:'24px',mb:'24px',py:'12px',height:'38px',color:'#722ED1',borderColor:'none',backgroundColor:'#F7F1FF'}}>
              <img src={Eye} alt="Dimon" style={{ width:'25.78px', height: '17.18px' }} />
              <Typography sx={{fontSize:'16px',ml:"12px"}} >
                <b>Why Qubly</b>
              </Typography>
              </Button>
            </Grid>
            <Grid item xs={12}>
            <Typography sx={{fontSize:'32px',color:'#160637'}} >
            <Box sx={{ fontWeight: 700}}>Get actionable insights from Big Data in 3 steps</Box>
          </Typography>
            </Grid>
 
            <Grid item xs={12} >
              <Typography sx={{fontSize:'16px',mt:'24px',color:'#8989A2'}} >
              <Box sx={{ fontWeight: 500}}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod </Box>
              <Box sx={{ fontWeight: 500}}>tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</Box>
            </Typography>
          
            </Grid>
            <Grid  container justifyContent={'center'} spacing={2}>
              
              <Grid item  sx={{mt:'24px'}} >
                <img src={Chart} alt="Dimon" style={{ width:'40px', height: '40px' }} />
                <Typography sx={{fontSize:'14px',color:'#160637',mt:'16px'}} >
                  <Box sx={{ fontWeight: 700}}>Valuable business insights</Box>
                </Typography>
                <Typography sx={{fontSize:'16px',mt:'16px',color:'#8989A2'}} >
                  <Box sx={{ fontWeight: 500}}>Collect processed & cleansed data </Box>
                  <Box sx={{ fontWeight: 500}}>that is ready to be analyzed to gather </Box>
                  <Box sx={{ fontWeight: 500}}>valuable business insights. </Box>
                </Typography>
              </Grid>
              <Grid item  sx={{mt:'24px'}} >
                <img src={Newsletter} alt="Dimon" style={{ width:'40px', height: '40px' }} />
                <Typography sx={{fontSize:'14px',color:'#160637',mt:'16px'}} >
                  <Box sx={{ fontWeight: 700}}>Powerful Algorithms</Box>
                </Typography>
                <Typography sx={{fontSize:'16px',mt:'16px',color:'#8989A2'}} >
                  <Box sx={{ fontWeight: 500}}>With the help of powerful algorithms,</Box>
                  <Box sx={{ fontWeight: 500}}>quality rules & techniques, obtain</Box>
                  <Box sx={{ fontWeight: 500}}>simplified & enriched data.</Box>
                </Typography>
              </Grid>
              <Grid item  sx={{mt:'24px'}} >
                <img src={Timeline} alt="Dimon" style={{ width:'40px', height: '40px' }} />
                <Typography sx={{fontSize:'14px',color:'#160637',mt:'16px'}} >
                  <Box sx={{ fontWeight: 700}}>Data in real-time</Box>
                </Typography>
                <Typography sx={{fontSize:'16px',mt:'16px',color:'#8989A2'}} >
                  <Box sx={{ fontWeight: 500}}>Collect data in real-time from multiple</Box>
                  <Box sx={{ fontWeight: 500}}>channels and move it into a data lake, in</Box>
                  <Box sx={{ fontWeight: 500}}>its original format.</Box>
                </Typography>
              </Grid>
              

            </Grid>
            <Grid container justifyContent={'center'} sx={{mt:'100px'}}>
            <Grid item xs={2}>
              </Grid>

              <Grid item xs={4}>
              <img src={GroupIcon} alt="Dimon" style={{ width:'412.5px', height: '435.5px' }} />
              </Grid>
              <Grid item xs={5} container  alignItems={'center'} >
                <Box sx={{display:'flex',flexDirection: 'column'}}>
                <Button  sx={{borderRadius:10,px:'24px',py:'12px',height:'38px',width:'224px',color:'#722ED1',borderColor:'none',backgroundColor:'#F7F1FF'}}>
                  <img src={GroupButton} alt="Dimon" style={{ width:'24px', height: '24px' }} />
                  <Typography sx={{fontSize:'16px',ml:"12px"}} >
                    <b>For Product Teams</b>
                  </Typography>
                </Button><br/>
                <Typography sx={{fontSize:'32px',display:'flex',color:'#160637'}} >
                  <Box sx={{ fontWeight: 700}}>Launch with the best stack</Box>
                    
                  </Typography>
                    <Typography sx={{fontSize:'16px',display:'flex',flexDirection: 'column',color:'#8989A2'}} >
                      <Box sx={{ fontWeight: 500,display:'flex'}}>A centralized platform that integrates zillions of data sources</Box>
                      <Box sx={{ fontWeight: 500,display:'flex'}}>using Big Data ELT (Extract, Load & Transform) that leaves</Box>
                      <Box sx={{ fontWeight: 500,display:'flex'}}>no data behind</Box>
                    </Typography>
               
                </Box>
                
                
              </Grid>
              <Grid item xs={1.5}>
              </Grid>
              <Grid item xs={3}  container  alignItems={'center'} >
                <Box sx={{display:'flex',flexDirection: 'column',mt:'225px'}}>
                <Button  sx={{borderRadius:10,px:'24px',py:'12px',height:'38px',width:'258px',color:'#722ED1',borderColor:'none',backgroundColor:'#F7F1FF'}}>
                  <img src={compass} alt="Dimon" style={{ width:'24px', height: '24px' }} />
                  <Typography sx={{fontSize:'16px',ml:"12px"}} >
                    <b>For Engineering Teams</b>
                  </Typography>
                </Button><br/>
                <Typography sx={{fontSize:'32px',display:'flex',color:'#160637'}} >
                  <Box sx={{ fontWeight: 700}}>Data-driven pipelines in minutes</Box>
                    
                  </Typography>
                    <Typography sx={{fontSize:'16px',display:'flex',flexDirection: 'column',color:'#8989A2'}} >
                      <Box sx={{ fontWeight: 500,display:'flex'}}>Maintenance-free data pipelines with quick set-up and</Box>
                      <Box sx={{ fontWeight: 500,display:'flex'}}>straight-forward deployments that are powered by a modern</Box>
                      <Box sx={{ fontWeight: 500,display:'flex'}}>& scalable platform.</Box>
                    </Typography>
               
                </Box>
                
                
              </Grid>
              <Grid item xs={4} sx={{mt:'121px'}}>
              <img src={GroupIcon2} alt="Dimon" style={{ width:'684px', height: '414px' }} />
              </Grid>
           
              <Grid item xs={12} sx={{mt:'1288px',zIndex:'1000'}}  position="absolute" >
                <Typography  sx={{fontSize:'24px',color:'#160637'}} >
                  <Box sx={{ fontWeight: 500}}>"What I love about Qubly is the easy way we can collaborate even if there is a "</Box>
                  <Box sx={{ fontWeight: 500}}>lot of people involved in the process"</Box>
                    
                  </Typography>
                  <img src={Profile} alt="Profile" style={{ width:'80px', height: '80px',marginTop:'32px',marginBottom:'32px' }} />

                  <Typography  sx={{fontSize:'16px',color:'#160637'}} >
                  <Box sx={{ fontWeight: 700}}>Guillaume Cabane</Box>
                  <Box sx={{ fontWeight: 700}}>CTO @ BigSpring</Box>

                  <Divider sx={{marginTop:'32px',marginBottom:'32px'}} />
                  <img src={Logo_5} alt="Logo_5" style={{ width:'943.38px', height: '38.02px' }} />
                    
                  </Typography>
                  
              </Grid>
              
              
              <Grid  xs={12} container justifyContent={'center'} height={'700px'} alignItems="flex-start" spacing={2} sx={{mt:'1073px'}} >
              <Grid item xs={1.5}>
              </Grid>
              <Grid item xs={3}  container  alignItems={'center'} sx={{mt:'42px'}} >
                <Box sx={{display:'flex',flexDirection: 'column',}}>
               
                <Typography sx={{fontSize:'32px',display:'flex',flexDirection: 'column',color:'#160637'}} >
                    <Box sx={{ fontWeight: 700,display:'flex'}}>See why the world’s best </Box>
                    <Box sx={{ fontWeight: 700,display:'flex'}}>companies use Qubly to</Box>
                    <Box sx={{ fontWeight: 700,display:'flex'}}>become truly data-driven.</Box>
                    
                  </Typography>
                  <Divider sx={{mt:'35px',mb:'50px'}}><Box sx={{fontSize:'16px',color:'#8989A2'}}>Trusted by</Box></Divider>
                  <img src={logo_4} alt="Dimon" style={{ width:'301.92px', height: '156.15px' }} />

               
                </Box>
                
                
              </Grid>
              <Grid item xs={4}  >
              
              <Box sx={{backgroundColor:'#F9F0FF',width:'480px', height: '480px',mt:'42px',mb:'50px',position:'absolute',borderRadius:'3px'}}>

              </Box>
              <Box sx={{backgroundColor:'#FFFFFF',width:'428px', height: '494px',ml:'26px',position:'absolute',boxShadow:'2',borderRadius:'3px'}}>
                <Box sx={{mx:'42px',mt:'32px',justifyContent:'center'}}>
                <Typography sx={{fontSize:'24px',mb:'16px',flexDirection: 'row',color:'#160637'}} >
                    <Box sx={{ fontWeight: 700}}>Sign Up</Box>
                  </Typography>
                  <Button fullWidth sx={{backgroundColor:'#722ED1',mb:'16px',color:'#fff',fontSize:'16px',borderRadius:'3px'}}>
                    Start your free trial
                  </Button>
                  <TextField fullWidth sx={{mb:'16px'}}  size='small' id="outlined-basic" label="Your primary email" />
                  <TextField fullWidth sx={{mb:'16px'}}  size='small' id="outlined-basic" label="Password"  />
                  <TextField fullWidth sx={{mb:'16px'}}  size='small' id="outlined-basic" label="Re-type Password"  />
                  <Typography sx={{fontSize:'16px',mb:'16px',flexDirection: 'row',color:'#000000'}} >
                    <Box sx={{ fontWeight: 700}}>OR</Box>
                  </Typography>
                  <Box>
                    
                  </Box>
                  <Box width={'100%'} display={'flex'} justifyContent={'center'}>
                    <GoogleButton
                      onClick={() => { console.log('Google button clicked') }}
                    />
                  </Box>
                  <Divider sx={{mt:'16px',mb:'16px'}}></Divider>
                  <Box width={'100%'} display={'flex'} justifyContent={'center'} >
                  <Typography sx={{fontSize:'16px',display:'flex'}} >
                    <Box sx={{ fontWeight: 500,color:'#8989A2'}}>Already have an account?</Box><span>&nbsp;</span>
                    <Box sx={{ fontWeight: 500,color:'#722ED1'}}>Login</Box>
                  </Typography>
                  </Box>
                </Box>
              
                </Box>
              </Grid>
              </Grid>
              
            </Grid>
            
          </Grid>
          


    </>
        
       
  );
}
export default ResponsiveAppBar;
