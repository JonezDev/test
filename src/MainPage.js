import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import Dimon from './img/dimon.png'
import MainImage from './img/MainImage.png'


function ResponsiveAppBar() {


  return (
    <>
        <Grid container>
        <Grid container  xs={12} sx={{ zIndex: '1000',width:'100%' }}>
            <Grid item xs={3}>

            </Grid>
            <Grid item xs={6} container justifyContent="flex-start" sx={{mt:'203px'}} >
              <Grid item sx={6}>
              <Box display={'flex'}  >
              <Button  sx={{borderRadius:10,px:'24px',mb:'24px',py:'12px',height:'38px',color:'#722ED1',borderColor:'none',backgroundColor:'#F7F1FF'}}>
              <img src={Dimon} alt="Dimon" style={{ width: 16, height: 16 }} />
              <Typography sx={{fontSize:'16px',ml:"12px"}} >
                <>v3.1 released. Learn more</>
              </Typography>
              </Button>
            
              </Box>
              <Box >
              <Typography sx={{fontSize:'48px',display:'flex'}} >
                <Box sx={{ fontWeight: 700}}>Your data with</Box>
              </Typography>
              </Box>
              <Box>
              <Typography sx={{fontSize:'48px',display:'flex',mt:-3,mb:1}} >
              <Box sx={{ fontWeight: 700}}>real-time analytics</Box>
              </Typography>
              </Box>
              <Box>
              <Typography sx={{fontSize:'16px',display:'flex',opacity:'50%'}} >
                <>Harness the potential of Big Data Analytics & Cloud Services </>
              </Typography>
              </Box>
              <Box>
              <Typography sx={{fontSize:'16px',display:'flex',opacity:'50%'}} >
                <>and become a data-driven organization with Needle tail </>
              </Typography>
              </Box>
              <Box display={'flex'} sx={{mt:5}}>
              <Button  sx={{borderRadius:10,px:'24px',mb:'24px',py:'12px',height:'38px',color:'#fff',borderColor:'none',backgroundColor:'#722ED1'}}>
              <Typography sx={{fontSize:'16px'}} >
                <>Start free trial</>
              </Typography>
              </Button>
              <Button  sx={{borderRadius:10,px:'24px',ml:'16px',mb:'24px',py:'12px',height:'38px',color:'#722ED1',borderColor:'none',backgroundColor:'#fff'}}>
              <Typography sx={{fontSize:'16px'}} >
                <>Learn more</>
              </Typography>
              </Button>
            
              </Box>
              </Grid>
              <Grid item xs={6} justifyContent="flex-start" >
              <img src={MainImage} alt="MainImage" style={{ width: 472, height: 370 }} />
              
            </Grid>
            </Grid>
            

            <Grid item xs={3}>

            </Grid>
            
          </Grid>
        </Grid>
        
          
          


    </>
        
       
  );
}
export default ResponsiveAppBar;
