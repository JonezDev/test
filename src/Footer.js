import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import Dimon from './img/dimon.png'
import MainImage from './img/MainImage.png'
import Logo from './img/Logo.png'
import Arrow from './img/arrow.png'
import Image_7 from './img/image_7.png'
import Twitter from './img/twitter.png'
import Instagram from './img/instagram.png'
import Facebook from './img/facebook.png'
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';

import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import DirectionsIcon from '@mui/icons-material/Directions';


function ResponsiveAppBar() {


  return (
    <>
    <Box position="absolute" width={'100%'} height={'400px'} sx={{mb:'0',background:'#FBFCFF',zIndex: '1000',justifyContent:'center'}}>
        <Grid   
          container
          direction="row"
          justifyContent="center"
          alignItems="flex-start"
          sx={{mt:'58px',mr:'240px'}}
          spacing={2}
        >
          <Grid 
            item xs={1.5} 
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="flex-start"
          >
          <img src={Logo} alt="Logo" style={{ width: 48, height: 48  }} />
          <Typography sx={{fontSize:'12px',display:'flex',flexDirection: 'column',color:'#160637'}} >
            <Box sx={{ fontWeight: 500,display:'flex',mt:'20px'}}>Lorem ipsum dolor sit amet,</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}}>consectetuer adipiscing elit, sed</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}}>diam nonummy nibh euismod</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}}>tincidunt ut laoreet dolore magna</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}}>aliquam erat volutpat ut wisi enim</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}}>ad minim</Box>
            
          </Typography>
          <Box sx={{mt:'10px'}}>
            <IconButton color="primary" sx={{ pr: '5px' }} aria-label="directions">
              <img src={Facebook} alt="Facebook" style={{ width: '16px', height: '16.01px' }} />
            </IconButton>
            <IconButton color="primary" sx={{ pr: '5px' }} aria-label="directions">
              <img src={Twitter} alt="Twitter" style={{ width: '16px', height: '16.01px' }} />
            </IconButton>
            <IconButton color="primary" sx={{ pr: '5px' }} aria-label="directions">
              <img src={Instagram} alt="Instagram" style={{ width: '16px', height: '16.01px' }} />
            </IconButton>
          </Box>
          
          
          </Grid>
          <Grid 
            item xs={1} 
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="flex-start"
          >
          <Typography sx={{fontSize:'16px',fontWeight: 700,display:'flex',flexDirection: 'column',color:'#160637'}} >
          <Box sx={{mb:'20px'}} >Product</Box>
          </Typography>
          <Typography sx={{fontSize:'12px',fontWeight: 500,display:'flex',flexDirection: 'column',color:'#160637'}} >
          <Box sx={{ fontWeight: 500,display:'flex'}} >Connections</Box>
          <Box sx={{ fontWeight: 500,display:'flex'}} >Protocols</Box>
          <Box sx={{ fontWeight: 500,display:'flex'}} >Personas</Box>
          <Box sx={{ fontWeight: 500,display:'flex'}} >Integrations</Box>
          <Box sx={{ fontWeight: 500,display:'flex'}} >Catalog</Box>
          <Box sx={{ fontWeight: 500,display:'flex'}} >Pricing</Box>
          <Box sx={{ fontWeight: 500,display:'flex'}} >Security</Box>
          <Box sx={{ fontWeight: 500,display:'flex'}} >GDPR</Box>
          </Typography>
          </Grid>
          <Grid 
            item xs={1} 
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="flex-start"
          >
          <Typography sx={{fontSize:'16px',fontWeight: 700,display:'flex',flexDirection: 'column',color:'#160637'}} >
          <Box sx={{mb:'20px'}} >For Developers</Box>
          </Typography>
          <Typography sx={{fontSize:'12px',fontWeight: 500,display:'flex',flexDirection: 'column',color:'#160637'}} >
            <Box sx={{ fontWeight: 500,display:'flex'}} >Docs</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >API</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Open Source</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Engineering Team</Box>
          </Typography>
          </Grid>
          <Grid 
            item xs={1} 
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="flex-start"
          >
          <Typography sx={{fontSize:'16px',fontWeight: 700,display:'flex',flexDirection: 'column',color:'#160637'}} >
          <Box sx={{mb:'20px'}}>Company</Box>
          </Typography>
          <Typography sx={{fontSize:'12px',fontWeight: 500,display:'flex',flexDirection: 'column',color:'#160637'}} >
            <Box sx={{ fontWeight: 500,display:'flex'}} >Careers</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Blog</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Press</Box>
          </Typography>
          </Grid>
          <Grid 
            item xs={1} 
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="flex-start"
          >
          <Typography sx={{fontSize:'16px',fontWeight: 700,display:'flex',flexDirection: 'column',color:'#160637'}} >
          <Box sx={{mb:'20px'}} >Support</Box>
          </Typography>
          <Typography sx={{fontSize:'12px',fontWeight: 500,display:'flex',flexDirection: 'column',color:'#160637'}} >
            <Box sx={{ fontWeight: 500,display:'flex'}} >Help Center</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Contact Us</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Bulletins</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Documentation</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Partner</Box>
            <Box sx={{ fontWeight: 500,display:'flex'}} >Portal</Box>
          </Typography>
          </Grid>
          <Grid 
            item xs={1} 
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="flex-start"
          >
          <Typography sx={{fontSize:'16px',fontWeight: 700,display:'flex',flexDirection: 'column',color:'#160637'}} >
          <Box>Newsletter</Box>
          </Typography>
          <Paper
            component="form"
            sx={{ p: '2px 4px',mt:'20px', display: 'flex', alignItems: 'center', width: '180px',borderRadius:'0px',boxShadow:'3' }}
          >
           
            <InputBase
              sx={{ ml: 1, flex: 1 }}
              placeholder="Email"
              inputProps={{ 'aria-label': 'Email' }}
            />
          
            <IconButton color="primary" sx={{ pr: '5px' }} aria-label="directions">
            <img src={Arrow} alt="Arrow" style={{ width: '19.81px', height: '19.8px' }} />
            </IconButton>
          </Paper>

          </Grid>
        </Grid>
        <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="flex-start"
        sx={{mt:'58px',mr:'240px'}}
        spacing={2}
        >
        <Typography sx={{fontSize:'16px',fontWeight: 700,display:'flex',flexDirection: 'column',color:'#160637'}} >
          <Box sx={{mr:'8px'}}>Rahul Rao</Box>
          </Typography>
          <img src={Image_7} alt="Image_7" style={{ width: 24, height: 24  }} />
        </Grid>
          
    </Box>      


    </>
        
       
  );
}
export default ResponsiveAppBar;
