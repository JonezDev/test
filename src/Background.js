import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import { Grid } from '@mui/material';
import BG from './img/BG.png'
import BG2 from './img/BG2.png'
import shadows from '@mui/material/styles/shadows';

const pages = ['About', 'Pricing', 'Contact Us','Login'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

function ResponsiveAppBar() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <>
          <Box position="absolute" sx={{zIndex:'0',width:'100%', height: '100%'}}>
            <img src={BG} alt="BG" style={{ width:'100%', height: '100%',zIndex:'0' }} /> 
          </Box>
          <Box position="absolute" sx={{zIndex:'0',width:'100%', height: '100%',mt:'2588px'}}>
            <img src={BG2} alt="BG2" style={{ width:'100%', height: '100%',zIndex:'0' }} /> 
          </Box>
    </>
        
       
  );
}
export default ResponsiveAppBar;
