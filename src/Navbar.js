import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import { Grid } from '@mui/material';
import Logo from './img/Logo.png'
import shadows from '@mui/material/styles/shadows';

const pages = ['About', 'Pricing', 'Contact Us','Login'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

function ResponsiveAppBar() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <AppBar position="absolute" elevation={0} style={{ background: 'none',zIndex:'tooltip' }}>
        <Grid container xs='12'
           
            direction="row"
            justifyContent="space-evenly"
            alignItems="center"
            sx={{px:15}}
        >
            <Grid item >
            
            <img src={Logo} alt="Logo" style={{ width: 48, height: 48 }} />
            </Grid>
            

            <Grid item
            >
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
           
         
          </Box>

          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {pages.map((page) => (
              <Button
                key={page}
                onClick={handleCloseNavMenu}
                sx={{ my:'19px', color: 'black', display: 'block',px:3}}
              >
               
               <Typography sx={{fontSize:'14px',}}><b>{page}</b></Typography>
              </Button>
            ))}
            <Box sx={{ my:'19px', color: 'black', display: 'block',px:3 }} >
                <Button variant="outlined" sx={{borderRadius:10,px:'24px',py:'12px',height:'38px',color:'#722ED1',borderColor:'#722ED1'}}><Typography sx={{fontSize:'14px'}} ><b>Start free trial</b></Typography></Button>
            </Box>

          </Box>

            </Grid>
          
        
        </Grid>
    </AppBar>
  );
}
export default ResponsiveAppBar;
